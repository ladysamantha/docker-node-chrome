# docker-node-chrome

_forked from [https://github.com/amio/docker-node-chrome](https://github.com/amio/docker-node-chrome)_

Dockerfile for nodejs + chrome.

## On docker hub

https://hub.docker.com/r/tenders/docker-node-chrome/

This [Dockerfile](/Dockerfile) contains:

- node: 8
- npm: (bundled with node 8)
- google-chrome: 67 (latest as of 13 June 2018)